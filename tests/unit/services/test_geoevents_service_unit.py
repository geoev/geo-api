'''
GeoEvent service unit tests
'''
import pytest
import json
from app.api.services.geoevents import GeoEventService

@pytest.mark.asyncio
async def test_create(conn):
    events = [
        {"lat": 23.83294, "lon": -82.29838, "ev_type": "random1", "src": "test"},
        {"lat": 23.83294, "lon": -82.29838, "ev_type": "random2", "src": "test",
         "key": "key1", "value": json.dumps({"empty": True})},
    ]
    gserv = GeoEventService()
    await gserv.create(events)

@pytest.mark.asyncio
async def test_create_returning(conn):
    events = [
        {"lat": 23.83294, "lon": -82.29838, "ev_type": "random1", "src": "test"},
        {"lat": 23.83294, "lon": -82.29838, "ev_type": "random2", "src": "test",
         "key": "key1", "value": json.dumps({"empty": True})},
    ]
    gserv = GeoEventService()
    await gserv.create_returning(events)

@pytest.mark.asyncio
async def test_get(conn):
    gserv = GeoEventService()
    results = await gserv.get()
    for result in results:
        print(F"result: {result}")

@pytest.mark.asyncio
async def test_delete(conn):
    gserv = GeoEventService()
    where = {"ev_type": "random2"}
    results = await gserv.delete(**where)
    print(json.dumps(results, indent=2))
