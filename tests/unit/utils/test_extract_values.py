import json
from app.utils import extract_values

def test_extract_values():
    data = [
        {'lat': 23.83294, 'lon': -82.29838, 'description': 'random'},
        {'lat': 23.83294, 'lon': -82.29838, 'description': 'random', 'key': 'key1', 'value': json.dumps({'empty': True})},
    ]
    keys = 'id lat lon description key value'.split(' ')
    excluded = ('id',)
    
    values = extract_values(data, keys, excluded)
    print(json.dumps(values, indent=2))
    _keys = [key for key in keys if key not in excluded]
    for datum, value in zip(data, values):
        for idx, key in enumerate(_keys):
            assert value[idx] == datum.get(key)
