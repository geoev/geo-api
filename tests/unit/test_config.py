'''
Unit test for config set() and get()
'''

def test_set(config):
    values = {
        'this': 'that',
        'these': 'those',
        'colors': ['red', 'blue', 'green'],
        'db.user': 'fred',
        'db.pwd': 'pwd',
        'db.host': 'localhost',
        'db.port': 5432,
        'db.name': 'geo_events',
    }

    for key, value in values.items():
        config.set(key, value, create=True)
        actual = config.get(key)
        assert actual == value, f"{key}: expected={value}, actual={actual}"

    db = config.get('db')
    for key in ['user', 'pwd', 'host', 'port', 'name']:
        assert getattr(db, key) == values[F"db.{key}"]

