'''
pytest fixtures
'''
from pytest_asyncio import fixture

from app.db import database
from app.config import Config

@fixture
async def conn():
    await database.connect()
    yield database
    await database.disconnect()

@fixture
def config():
    config = Config('GEO_')
    return config
