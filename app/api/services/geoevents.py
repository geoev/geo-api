'''
GeoEvents to/from the db:
'''
from geo_db.models.geo_event import geo_event_table

from app.db import database as db
from app.utils import populate_missing
from app.errors import DatabaseError
from app.logger import log

class GeoEventService:
    fields = [col.name for col in geo_event_table.c]

    async def create(self, events):
        populate_missing(events, self.fields, ('id',))

        fields = [field for field in self.fields if field != 'id']
        values = [F":{field}" for field in fields]
        fields = ', '.join(fields)
        values = ', '.join(values)
        sql = F"INSERT INTO geo_event ({fields}) VALUES ({values})"
        async with db.connection() as conn:
            async with conn.transaction():
                try:
                    await conn.execute_many(query=sql, values=events)
                except Exception as e:  # this should be more specific; 
                    raise DatabaseError(e)
        return {'n_created': len(events)}

    async def create_returning(self, events):
        populate_missing(events, self.fields, ('id',))
        fields = [field for field in self.fields if field != 'id']
        colon_fields = ', '.join(F":{field}" for field in fields)
        fields = ', '.join(fields)
        sql = F"INSERT INTO geo_event ({fields}) VALUES ({colon_fields}) RETURNING *"
        async with db.connection() as conn:
            async with conn.transaction():
                await conn.execute_many(query=sql, values=events)

    async def get(self, **where):
        fields = ', '.join(self.fields)
        sql = F"SELECT {fields} FROM geo_event"
        if where:
            where_clause = ' AND '.join(F"{key}=:{key}" for key in where.keys())
            sql += F" WHERE {where_clause}"
        async with db.connection():  # is this correct???
            results = await db.fetch_all(sql, values=where)
        return [dict(res) for res in results]

    async def delete(self, **where):
        sql = "DELETE FROM geo_event"
        if where:
            where_clause = ' AND '.join(F"{key}=:{key}" for key in where.keys())
            sql += F" WHERE {where_clause}"
        sql += " RETURNING *"
        async with db.connection() as conn:
            async with conn.transaction():
                results = await db.fetch_all(sql, values=where)
        return [dict(res) for res in results]
