from fastapi import APIRouter, Depends
from app.security import oauth2_scheme
from app.config import config

# routes:
router = APIRouter(prefix='/config')

@router.get("/", name='all_config')
async def all_config(
        token: str = Depends(oauth2_scheme),  # unused for now
):
    ''' return the config, but only to approved roles '''
    return config.__dict__

@router.patch("/", name='patch_config')
async def patch_config(
        path: str, value: str, create: bool,
        token: str = Depends(oauth2_scheme),  # unused for now
):
    pass
