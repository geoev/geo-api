from fastapi import APIRouter, Depends
from geo_db.models.geo_event import CreateGeoEvents
from app.api.services.geoevents import GeoEventService
from app.security import oauth2_scheme

router = APIRouter(prefix='/geoevents')
ge_service = GeoEventService()

@router.get("/", name='search_geoevents')
async def search(src=None, ev_type=None, key=None,
                 token: str = Depends(oauth2_scheme),  # unused
                 ):
    ''' search on description or key '''
    where = {'src': src, 'ev_type': ev_type, 'key': key}
    where = {key: val for key, val in where.items() if val is not None}
    events = await ge_service.get(**where)
    return events

@router.post("/", name='create_geoevents')
async def create_many(events: CreateGeoEvents):
    ''' create multiple GeoEvents '''
    ev_data = events.dict()['events']
    return await ge_service.create(ev_data)
    
