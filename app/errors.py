class DatabaseError(Exception):
    orig_type: str
    orig_msg: str

    def __init__(self, exc: Exception):
        self.orig_type = type(exc)
        self.orig_msg = str(exc)

    def __str__(self):
        return self.orig_msg
