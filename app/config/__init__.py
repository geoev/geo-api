import os
import yaml
import pkg_resources as pr
from types import SimpleNamespace

class Config(SimpleNamespace):
    ''' . '''
    def __init__(self, env_prefix: str, config_yaml: str=None):
        if config_yaml:
            yml = self._load_yaml(config_yaml)
            self.merge(yml)
        self._load_env(env_prefix)

    def _load_env(self, env_prefix):
        for key, value in os.environ.items():
            if key.startswith(env_prefix):
                key = key.replace(env_prefix, '')
                value = Config.type_convert(value)
                self.set(key, value, create=True)

    def _load_yaml(self, config_yaml):
        with open(config_yaml) as yml:
            return yaml.safe_load(yml)

    def merge(self, other):
        if hasattr(other, '__dict__'):
            self.__dict__.update(other.__dict__)
        elif isinstance(other, dict):
            self.__dict__.update(other)
        else:
            raise TypeError(type(other))

    @staticmethod
    def type_convert(value):
        if value.lower() == 'true':
            return True
        if value.lower() == 'false':
            return False
        try:
            return int(value)
        except Exception:
            pass
        try:
            return float(value)
        except Exception:
            pass
        return value

    def load(self, fn):
        pass

    def get(self, path):
        attr = self
        anames = path.split('.')
        for aname in anames:
            attr = getattr(attr, aname)
        return attr

    def set(self, path, value, create=False):
        attr = self
        anames = path.split('.')
        for aname in anames[:-1]:
            attr = Config._get_or_create_attr(attr, aname, create)

        setattr(attr, anames[-1], value)

    @staticmethod
    def _get_or_create_attr(attr, aname, create):
        if not hasattr(attr, aname):
            if not create:
                raise ValueError(f"config: {aname} doesn't exist")
            newattr = SimpleNamespace()
            setattr(attr, aname, newattr)
        else:
            newattr = getattr(attr, aname)
        return newattr

# _config_yaml will have to change when deploying to cloud
_config_yaml = pr.resource_filename('app', 'config/config.yaml')
config = Config('GEO_', _config_yaml)
