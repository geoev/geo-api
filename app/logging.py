import os
import pkg_resources as pr
import logging
import logging.config


log = logging.getLogger(__name__)
lpath = pr.resource_filename(__name__, 'config/logging.conf')
assert os.path.exists(lpath), f"{lpath}: no such file"
logging.config.fileConfig(lpath)
log.setLevel(logging.DEBUG)

logging.getLogger('sqlalachemy.engine').setLevel(logging.INFO)
