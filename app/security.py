import datetime as dt
from http import HTTPStatus
from passlib.context import CryptContext
from jose import JWTError, jwt

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from app.api.models.user import User, UserInDB, TokenData
from app.config import config
from app.logger import log

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

fake_users_db = {
    "johndoe": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
        "active": True,
    },
    "alice": {
        "username": "alice",
        "full_name": "Alice Wonderson",
        "email": "alice@example.com",
        "hashed_password": "fakehashedsecret2",
        "active": True
    },
}

pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')
SECRET_KEY = config.SECRET_KEY
ALGORITHM = config.ALGORITHM
ACCESS_TOKEN_EXPIRES_MINUTES = config.ACCESS_TOKEN_EXPIRES_MINUTES


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return pwd_context.hash(password)

def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)

def authenticate_user(db, username: str, password: str):
    user = get_user(db, username)
    if not user:
        log.debug("authenticate_user: no user")
        return False
    if not verify_password(password, user.hashed_password):
        log.debug(F"authenticate_user: {username} password mis-match")
        return False
    return user

def create_access_token(data: dict, expires_delta: dt.timedelta):
    to_encode = data.copy()
    expires_delta = expires_delta or 15 * 60  # 15 minutes
    expire = dt.datetime.utcnow() + expires_delta
    to_encode['exp'] = expire
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

async def get_current_user(token: str = Depends(oauth2_scheme)):
    ''' return a fake user (for now) '''
    # oauth2_scheme self-hits /token, which (currently) gets user data from
    # form_data: OAuth2PasswordRequestForm, then looks up user in fake_users_db
    # (or raises HTTPStatus.UNAUTHORIZED)
    credentials_exp = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid user credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    except JWTError as e:
        log.debug(F"caught {type(e)}: {e}")
        raise credentials_exp

    username: str = payload.get("sub")
    if username is None:
        raise credentials_exp
    token_data = TokenData(username=username)

    user = get_user(fake_users_db, username=token_data.username)
    if user is None:
        log.debug("no current user")
        raise credentials_exp
    return user

async def get_current_active_user(
        current_user: User = Depends(get_current_user)):
    ''' . '''
    if not current_user.active:
        raise HTTPException(status_code=HTTPStatus.UNAUTHORIZED)
    return current_user
