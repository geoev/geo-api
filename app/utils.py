'''
GeoEvent utils
'''

def extract_values(data, keys, excluded=None):
    '''
    Given a list of dicts 'data' and a list of keys, return a list of tuples where each
    tuple is the extracted dict values in the order determined by 'keys'.
    Exclude any keys listed in 'excluded',
    @param data: list of dicts
    @param keys: list of keys
    @param excluded: list of keys
    '''
    all_values = []
    if excluded is None:
        excluded = tuple()
    for datum in data:
        values = tuple(datum.get(key) for key in keys if key not in excluded)
        all_values.append(values)
    return all_values

def populate_missing(data, fields, excluded=None, default=None):
    '''
    For a list of dicts and a list of keys, make sure each dict has all keys
    '''
    for datum in data:
        for field in fields:
            if field not in excluded:
                datum.setdefault(field, default)
