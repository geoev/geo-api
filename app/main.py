'''
GeoEvent API main module
'''
import datetime as dt
import time
from fastapi import FastAPI, Request, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.responses import JSONResponse

from app import version
from app.db import database
from app.api.models.user import User, Token
from app.api.routers.geoevents import router as ge_router
from app.api.routers.config import router as config_router
from app.security import fake_users_db, get_current_active_user
from app.security import authenticate_user, create_access_token
from app.security import ACCESS_TOKEN_EXPIRES_MINUTES
from app.logger import log
from app.errors import DatabaseError

app = FastAPI()
app.include_router(ge_router)
app.include_router(config_router)

@app.on_event("startup")
async def on_startup():
    await database.connect()
    log.info('connected to db')

@app.on_event("shutdown")
async def on_shutdown():
    await database.disconnect()

@app.get("/version")
async def root():
    return {"version": version}

@app.post("/token", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    '''
    On success, return a dict with an access_token and token_type.
    On failure, raise UNAUTHORIZED exception
    '''
    # success is determined by a authenticate_user():
    user = authenticate_user(fake_users_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"})
    access_token_expires = dt.timedelta(minutes=ACCESS_TOKEN_EXPIRES_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username},
        expires_delta=access_token_expires
    )
    return {'access_token': access_token, 'token_type': 'bearer'}


@app.get("/users/me", response_model=User)
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    return current_user

@app.exception_handler(DatabaseError)
async def handle_database_error(request, exc):
    ''' catch any errors that weren't caught by other handlers '''
    log.exception(exc)
    log.info(str(request))
    content = {'exc_type': str(type(exc)), 'exc': str(exc)}
    name = getattr(exc, 'name', None)
    if name:
        content['name'] = name
    return JSONResponse(
        status_code=500,
        content=content
    )

@app.middleware('http')
async def log_duration(request: Request, call_next):
    t0 = time.time()
    response = await call_next(request)
    duration = time.time() - t0
    log.info(F"{request.url} {response.status_code}: {duration:.5}s")
    return response
    
