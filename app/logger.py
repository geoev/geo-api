import sys
import logging

log = logging.getLogger('app')
log.setLevel(logging.DEBUG)

handler = logging.StreamHandler(stream=sys.stdout)
fmt = '%(levelname)s %(filename)s:%(lineno)s %(message)s'
handler.setFormatter(logging.Formatter(fmt))
log.addHandler(handler)
