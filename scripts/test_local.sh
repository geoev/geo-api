#!/bin/bash

source scripts/env_prepare.sh
pytest -s --asyncio-mode=strict $1
